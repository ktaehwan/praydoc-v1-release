package com.nazlab.praydoc.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nazlab.praydoc.R;
import com.nazlab.praydoc.model.PrayModel;
import com.nazlab.praydoc.util.Constant;

public class CustomAdapter extends BaseAdapter{
	private LayoutInflater inflater;
	private ArrayList<PrayModel> prayList;
	private String prayId;
	private Typeface typeFace = null;
	
	public CustomAdapter(Context c , ArrayList<PrayModel> array){
		this.inflater = LayoutInflater.from(c);
		this.prayList = array;
		this.typeFace = Typeface.createFromAsset(c.getAssets(), Constant.FONT);
	}
	
	@Override
	public int getCount() {
		int listCnt = prayList.size();
		return listCnt;
	}

	@Override
	public Object getItem(int position) {
		return prayList.get(position).getId();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if(convertView == null) {
			convertView = inflater.inflate(R.layout.pray_list_row, parent, false);
		}
		
		TextView id = (TextView) convertView.findViewById(R.id.pray_id);
		prayId = "" + prayList.get(position).getId();
		id.setText(prayId);
		
		TextView date = (TextView) convertView.findViewById(R.id.text_date);
		date.setTypeface(typeFace, Typeface.BOLD);
		date.setText(prayList.get(position).getDate());
		
		return convertView;
	}
}
