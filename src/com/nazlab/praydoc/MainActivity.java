package com.nazlab.praydoc;

import java.util.ArrayList;
import java.util.List;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.nazlab.praydoc.adapter.PrayPagerAdapter;
import com.nazlab.praydoc.fragment.MainMenuFragment;
import com.nazlab.praydoc.fragment.PrayListFragment;
import com.nazlab.praydoc.fragment.SponFragment;
import com.nazlab.praydoc.fragment.TodayPrayFragment;
import com.nazlab.praydoc.util.PrayViewPager;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class MainActivity extends FragmentActivity {
	private PrayPagerAdapter mPagerAdapter;
	private PrayViewPager mPager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		this.initailisePaging();
		
		Intent intent = new Intent();
		intent.setClass(this, SplashActivity.class);
		startActivity(intent);
	}
	
	private void initailisePaging() {
		List<Fragment> fragments = new ArrayList<Fragment>();
		fragments.add(Fragment.instantiate(this, PrayListFragment.class.getName()));
		fragments.add(Fragment.instantiate(this, MainMenuFragment.class.getName()));
		fragments.add(Fragment.instantiate(this, TodayPrayFragment.class.getName()));
		
		this.mPagerAdapter  = new PrayPagerAdapter(getSupportFragmentManager(), fragments);
		mPager = (PrayViewPager)findViewById(R.id.pager_view);
		mPager.setAdapter(this.mPagerAdapter);
		mPager.setCurrentItem(1, false);
	}
	
	public String getPreferences(String key) {
		SharedPreferences pref = getSharedPreferences("pray", Context.MODE_PRIVATE);
		return pref.getString(key, "defValue");
	}

	public void savePreference(String key, String value) {
		SharedPreferences pref = getSharedPreferences("pray", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	public void isPagingEnable(boolean isPagingEnabled) {
		mPager.setPagingEnabled(isPagingEnabled);
	}
}
