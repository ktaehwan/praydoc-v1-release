package com.nazlab.praydoc.model;

public class PrayModel {
	private int id;
	private String date;
	private String thank;
	private String repent;
	private String bless;
	private String want;
	
	public PrayModel(int id, String date, String thank, String repent, String bless, String want) {
		this.id = id;
		this.date 	= date;
		this.thank 	= thank;
		this.repent = repent;
		this.bless 	= bless;
		this.want 	= want;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getThank() {
		return thank;
	}
	public void setThank(String thank) {
		this.thank = thank;
	}
	public String getRepent() {
		return repent;
	}
	public void setRepent(String repent) {
		this.repent = repent;
	}
	public String getBless() {
		return bless;
	}
	public void setBless(String bless) {
		this.bless = bless;
	}
	public String getWant() {
		return want;
	}
	public void setWant(String want) {
		this.want = want;
	}
}
