package com.nazlab.praydoc.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.nazlab.praydoc.util.Constant;

public class DbOpenHelper {
	public static SQLiteDatabase mDB;
	private DatabaseHelper mDBHelper;
	private Context mCtx;
	private final int COLUMN_CNT = 0;
	
	public DbOpenHelper(Context context){
		this.mCtx = context;
	}

	public DbOpenHelper open() throws SQLException{
		mDBHelper = new DatabaseHelper(mCtx, Constant.DB_NAME, null, Constant.DB_VERSION);
		mDB = mDBHelper.getWritableDatabase();
		return this;
	}

	public void close(){
		mDB.close();
	}

	// Insert DB
	public long insertColumn(ContentValues values){
		return mDB.insert(Constant.TABLE_NAME, null, values);
	}

	// Delete ID
	public boolean deleteColumn(long id){
		boolean resultValue = false;
		
		if(mDB.delete(Constant.TABLE_NAME, "_id="+id, null) > COLUMN_CNT) {
			resultValue = true;
		}
		
		return resultValue;
	}
	
	// Select All
	public Cursor getAllColumns(){
		return mDB.query(Constant.TABLE_NAME, null, null, null, null, null, "DATE desc");
	}

	public Cursor getColumn(long id){
		Cursor c = mDB.query(Constant.TABLE_NAME, null, "_id="+id, null, null, null, null);

		if(c != null && c.getCount() != COLUMN_CNT) {
			c.moveToFirst();
		}
		return c;
	}
	
	private class DatabaseHelper extends SQLiteOpenHelper{
		public DatabaseHelper(Context context, String name,	CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(Constant.CREATE_TABLE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS "+Constant.TABLE_NAME);
			onCreate(db);
		}
	}

}
