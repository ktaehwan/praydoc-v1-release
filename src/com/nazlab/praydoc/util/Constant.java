package com.nazlab.praydoc.util;

public interface Constant {
	public static final String DB_NAME 		= "praydoc.db";
	public static final String TABLE_NAME 	= "praydoc";
	public static final int DB_VERSION 		= 1;
	public static final String CREATE_TABLE	= "CREATE TABLE "+Constant.TABLE_NAME
											+ " (" 
											+ " _id INTEGER PRIMARY KEY AUTOINCREMENT "
											+ ", "	+Constant.DATE+ 	" TEXT "
											+ ", "	+Constant.THANK+ 	" TEXT "
											+ ", "	+Constant.REPENT+ 	" TEXT "
											+ ", "	+Constant.BLESS+ 	" TEXT "
											+ ", "	+Constant.WANT+ 	" TEXT " 
											+ " ); ";
	
	public static final String DATE 	= "DATE";
	public static final String THANK 	= "THANK";
	public static final String REPENT 	= "REPENT";
	public static final String BLESS 	= "BLESS";
	public static final String WANT 	= "WANT";
	public static final String DEFAULT 	= "defValue";
	public static final String TAG 	= "com.nazlab.praydoc";
	public static final String FONT = "fonts/GothamRnd-Light.otf";
	public static final String LAST_COMMENT 	= "예수님의 이름으로 기도합니다.";
	public static final String DELETE_COMMENT 	= "삭제되었습니다.";
	public static final String SAVE_COMMENT 	= "저장되었습니다.";
	public static final String PRAYDOC_ID 	= "PRAYDOC_ID";
	
	// MENU TEXT LINE COLOR
	public static final int LINE_COLOR 	= 0xFF696969;
}
