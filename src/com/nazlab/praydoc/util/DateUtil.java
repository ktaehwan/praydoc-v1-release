package com.nazlab.praydoc.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateUtil {
	public String currentDate() {
		Calendar calendar = Calendar.getInstance();
        java.util.Date date = calendar.getTime();
        String today = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
        
        return today;
	}
	
	public String getCurrentTime() {
		String time = null;
		Calendar calendar = Calendar.getInstance();
		DecimalFormat decimalFormat = new DecimalFormat("00");
		int year = calendar.get(Calendar.YEAR) % 100;
		int month = calendar.get(Calendar.MONTH) + 1;
		int date = calendar.get(Calendar.DATE);
		time = decimalFormat.format(year) + "." + decimalFormat.format(month) + "." + decimalFormat.format(date);
		
		return time;
	}
}
