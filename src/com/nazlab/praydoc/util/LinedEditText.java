package com.nazlab.praydoc.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.EditText;

public class LinedEditText extends EditText {
    private Paint mPaint;

    // we need this constructor for LayoutInflater
    public LinedEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(2);
        mPaint.setColor(Constant.LINE_COLOR);
    }
    
    public void setColor(int color) {
    	mPaint.setColor(color);
    }
    
    /** This is called to draw the LinedEditText object */
    @Override
    protected void onDraw(Canvas canvas) {
    	 int left = getLeft();
         int right = getRight();
         int paddingTop = getPaddingTop();
         int paddingBottom = getPaddingBottom();
         int height = getHeight();
         int lineHeight = getLineHeight();
         int count = (height - paddingTop - paddingBottom) / lineHeight;

         // If number of lines is larger than lines of height of EditText,
         // use number of lines instead
         int lineCount = getLineCount();
         
         if(count < lineCount) {
        	 count = lineCount;
         }

         // Draws one line in the rectangle for every line of text in the EditText
         for (int i = 0; i < count; i++) {
             // Gets the baseline
             int baseline = lineHeight * (i + 1) + paddingTop;
             // Draws a line in the background from the left of the rectangle
             // to the right, at a vertical position one dip below the baseline
             canvas.drawLine(left, baseline, right, baseline, mPaint);
         }
         
        super.onDraw(canvas);
    }
} 