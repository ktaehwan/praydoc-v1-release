package com.nazlab.praydoc.fragment;

import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nazlab.praydoc.MainActivity;
import com.nazlab.praydoc.R;
import com.nazlab.praydoc.db.DbOpenHelper;
import com.nazlab.praydoc.model.PrayModel;
import com.nazlab.praydoc.util.Constant;

public class PrayListDetailFragment extends Fragment {
	private DbOpenHelper mDbOpenHelper = null;
	private PrayModel prayModel = null;
	private TextView dateView = null;
	private TextView prayTextView = null;
	private ImageView recycleView = null;
	private LinearLayout recycleLayout = null;
	private ImageView delyesView = null;
	private ImageView delnoView = null;
	private ImageView closeView = null;
	private Typeface typeFace = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.pray_list_detail, container, false);	
		long prayId = getArguments().getLong(Constant.PRAYDOC_ID);
		connectDB(prayId);
		createView(view);
		return view;
	}
	
	private void createView(View view) {
		dateView = (TextView) view.findViewById(R.id.textDate);
		typeFace = Typeface.createFromAsset(getActivity().getAssets(), Constant.FONT);
		dateView.setTypeface(typeFace, Typeface.BOLD);
		
		recycleView = (ImageView)view.findViewById(R.id.button_recycle);
		recycleView.setOnClickListener(recycleListener);
		prayTextView = (TextView)view.findViewById(R.id.pray_text);
		recycleLayout = (LinearLayout)view.findViewById(R.id.layout_del);
		delyesView = (ImageView)view.findViewById(R.id.button_delyes);
		delyesView.setOnClickListener(delyesListener);
		delnoView = (ImageView)view.findViewById(R.id.button_delno);
		delnoView.setOnClickListener(delnoListener);
		closeView = (ImageView)view.findViewById(R.id.closeView);
		closeView.setOnClickListener(closeListener);
		
		if(prayModel != null) {		
			dateView.setText(prayModel.getDate());
			StringBuilder builder = new StringBuilder();
			String sThank 	= prayModel.getThank();
			String sRepent 	= prayModel.getRepent();
			String sBless 	= prayModel.getBless();
			String sWant 	= prayModel.getWant();
			if(sThank != null) {
				builder.append(sThank);
				builder.append("\r\n");
			}
			if(sRepent != null) {
				builder.append(sRepent);
				builder.append("\r\n");
			}
			if(sBless != null) {
				builder.append(sBless);
				builder.append("\r\n");
			}
			if(sWant != null) {
				builder.append(sWant);
				builder.append("\r\n");
			}
			
			builder.append("\r\n");
			builder.append(Constant.LAST_COMMENT);
			prayTextView.setText(builder.toString());
		}		
	}
	
	private OnClickListener recycleListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(recycleLayout.getVisibility() == View.GONE) {
				recycleLayout.setVisibility(View.VISIBLE);
				recycleView.setVisibility(View.GONE);
			}
		}
	};
	
	private OnClickListener delyesListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(recycleLayout.getVisibility() == View.VISIBLE) {
				boolean result = mDbOpenHelper.deleteColumn(prayModel.getId());
				if(result) {
					Toast.makeText(getActivity().getApplicationContext(), Constant.DELETE_COMMENT, Toast.LENGTH_SHORT).show();
					PrayListFragment prayListFragment = new PrayListFragment();
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					ft.replace(R.id.pray_list_main, prayListFragment);
					ft.commit();
				}
			}
		}
	};

	private OnClickListener delnoListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(recycleLayout.getVisibility() == View.VISIBLE) {
				recycleLayout.setVisibility(View.GONE);
				recycleView.setVisibility(View.VISIBLE);
			}
		}
	};
	
	private OnClickListener closeListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			getFragmentManager().popBackStack();
		}
	};
	
	@Override
	public void onDestroyView() {
		if(typeFace != null) {
			typeFace = null;
		}
		
		((MainActivity)getActivity()).isPagingEnable(true);
		
		if(mDbOpenHelper != null) {
			mDbOpenHelper.close();
			mDbOpenHelper = null;
		}
		super.onDestroyView();
	}
	
	private void connectDB(long prayId) {
		mDbOpenHelper = new DbOpenHelper(getActivity().getApplicationContext());
		mDbOpenHelper.open();
		Cursor mCursor = null;
		try {
			mCursor = mDbOpenHelper.getColumn(prayId);
			prayModel = new PrayModel(
					mCursor.getInt(mCursor.getColumnIndex("_id")),
					mCursor.getString(mCursor.getColumnIndex(Constant.DATE)),
					mCursor.getString(mCursor.getColumnIndex(Constant.THANK)),
					mCursor.getString(mCursor.getColumnIndex(Constant.REPENT)),
					mCursor.getString(mCursor.getColumnIndex(Constant.BLESS)),
					mCursor.getString(mCursor.getColumnIndex(Constant.WANT))
					);
		} catch(Exception e) {
			Log.e(Constant.TAG, ""+e);
		} finally {
			mCursor.close();
		}
	}
}
