package com.nazlab.praydoc.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nazlab.praydoc.MainActivity;
import com.nazlab.praydoc.R;

public class SponFragment extends Fragment {
	private ImageView closeView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_spon, container, false);
		createView(view);
		return view;
	}
	
	private void createView(View view) {
		closeView = (ImageView) view.findViewById(R.id.closeView);
		closeView.setOnClickListener(closeListener);
	}
	
	private OnClickListener closeListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			getFragmentManager().popBackStack();
		}
	};
	
	@Override
	public void onDestroyView() {
		((MainActivity)getActivity()).isPagingEnable(true);
		super.onDestroyView();
	}
}
