package com.nazlab.praydoc.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.nazlab.praydoc.MainActivity;
import com.nazlab.praydoc.R;
import com.nazlab.praydoc.util.Constant;
import com.nazlab.praydoc.util.LinedEditText;

public class WantFragment extends Fragment {
	private ImageView okButton;
	private LinedEditText editText;
	
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			settingKeyBoard();
		}
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.edit_want, container, false);
		createView(view);
		return view;
	}
	
	private void createView(View view) {
		okButton = (ImageView) view.findViewById(R.id.button_ok);
		okButton.setOnClickListener(clickListener);
		editText = (LinedEditText)view.findViewById(R.id.edit_want);
		editText.setColor(Constant.LINE_COLOR);
		String wantText = ((MainActivity)getActivity()).getPreferences(Constant.WANT);
		
		if(!wantText.equals(Constant.DEFAULT)) {
			editText.setText(wantText);
		}
		editText.requestFocus();
		handler.sendEmptyMessage(0);
	}
	
	private void settingKeyBoard() {
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(editText, 0);
	}
	
	private OnClickListener clickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if(editText.getText() != null) {
				((MainActivity)getActivity()).savePreference(Constant.WANT, editText.getText().toString());
			}
			InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
			getFragmentManager().popBackStack();
		}
	};
	
	@Override
	public void onDestroyView() {
		// 좌우 스크롤 되게 하기 위해 추가
		((MainActivity)getActivity()).isPagingEnable(true);
		super.onDestroyView();
	}

}
