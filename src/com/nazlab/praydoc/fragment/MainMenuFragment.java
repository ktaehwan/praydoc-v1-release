package com.nazlab.praydoc.fragment;

import android.annotation.TargetApi;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nazlab.praydoc.MainActivity;
import com.nazlab.praydoc.R;
import com.nazlab.praydoc.util.Constant;
import com.nazlab.praydoc.util.DateUtil;

@TargetApi(Build.VERSION_CODES.HONEYCOMB) 
public class MainMenuFragment extends Fragment {
	private ImageView thankView = null;
	private ImageView repentView = null;
	private ImageView blessView = null;
	private ImageView wantView = null;
	private TextView dateView = null;
	private ImageView menusponView = null;
	
	private Typeface typeFace = null;
	private DateUtil dateUtil = new DateUtil();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_mainview, null);
		createView(view);
		return view;
	}

	private void createView(View view) {
		thankView = (ImageView) view.findViewById(R.id.menu_thank);
		thankView.setOnClickListener(thankListener);
		
		repentView = (ImageView)view.findViewById(R.id.menu_repent);
		repentView.setOnClickListener(repentListener);
		
		blessView = (ImageView)view.findViewById(R.id.menu_bless);
		blessView.setOnClickListener(blessListener);
		
		wantView = (ImageView)view.findViewById(R.id.menu_want);
		wantView.setOnClickListener(wantListener);
		
		dateView = (TextView)view.findViewById(R.id.text_date);
		String date = dateUtil.getCurrentTime();
		typeFace = Typeface.createFromAsset(getActivity().getAssets(), Constant.FONT);
		dateView.setTypeface(typeFace, Typeface.BOLD);
		dateView.setText(date);
		
		menusponView = (ImageView)view.findViewById(R.id.menu_spon);
		menusponView.setOnClickListener(sponListener);
	}
	
	private OnClickListener thankListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// �޴� Ŭ���� �¿� ��ũ�� �������� �߰�
			((MainActivity)getActivity()).isPagingEnable(false);
			
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ThankFragment thankFragment = new ThankFragment();
			ft.replace(R.id.rootLayout, thankFragment);
			ft.addToBackStack(null);
			ft.commit();	
		}
	};
	
	private OnClickListener repentListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// �޴� Ŭ���� �¿� ��ũ�� �������� �߰�
			((MainActivity)getActivity()).isPagingEnable(false);
			
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			RepentFragment repentFragment = new RepentFragment();
			ft.replace(R.id.rootLayout, repentFragment);
			ft.addToBackStack(null);
			ft.commit();	
		}
	};

	private OnClickListener blessListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// �޴� Ŭ���� �¿� ��ũ�� �������� �߰�
			((MainActivity)getActivity()).isPagingEnable(false);
			
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			BlessFragment blessFragment = new BlessFragment();
			ft.replace(R.id.rootLayout, blessFragment);
			ft.addToBackStack(null);
			ft.commit();	
		}
	};

	private OnClickListener wantListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			((MainActivity)getActivity()).isPagingEnable(false);
						
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			WantFragment wantFragment = new WantFragment();
			ft.replace(R.id.rootLayout, wantFragment);
			ft.addToBackStack(null);
			ft.commit();	
		}
	};
	
	private OnClickListener sponListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			((MainActivity)getActivity()).isPagingEnable(false);
						
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			SponFragment sponFragment = new SponFragment();
			ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
			ft.replace(R.id.rootLayout, sponFragment);
			ft.addToBackStack("spon");
			ft.commit();
		}
	};
	
	@Override
	public void onDestroyView() {
		if(typeFace != null) {
			typeFace = null;
		}
		super.onDestroyView();
	}
}




