package com.nazlab.praydoc.fragment;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.nazlab.praydoc.MainActivity;
import com.nazlab.praydoc.R;
import com.nazlab.praydoc.adapter.CustomAdapter;
import com.nazlab.praydoc.db.DbOpenHelper;
import com.nazlab.praydoc.model.PrayModel;
import com.nazlab.praydoc.util.Constant;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PrayListFragment extends Fragment implements AdapterView.OnItemClickListener{
	private ListView listView;
	private DbOpenHelper mDbOpenHelper;
	private Cursor mCursor;
	private ArrayList<PrayModel> prays = null;
	private CustomAdapter mAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.pray_list_main, container, false);	
		createView(view);
		return view;
	}
	
	private void createView(View view) {
		listView = (ListView) view.findViewById(R.id.prayListView);
		// DB Create and Open
        mDbOpenHelper = new DbOpenHelper(getActivity().getApplicationContext());
        mDbOpenHelper.open();
        
        prays = getAllPrayList();
        
        mAdapter = new CustomAdapter(getActivity().getApplicationContext(), prays);
        mAdapter.notifyDataSetChanged();
        listView.setAdapter(mAdapter);
		listView.setOnItemClickListener(this);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		((MainActivity)getActivity()).isPagingEnable(false);
		
		PrayListDetailFragment prayListDetailFragment = new PrayListDetailFragment();
		TextView prayId = (TextView)view.findViewById(R.id.pray_id);
		String strId = "" + prayId.getText();
		Long longId = Long.parseLong(strId);
		
		Bundle bundle = new Bundle();
		bundle.putLong(Constant.PRAYDOC_ID, longId);
		prayListDetailFragment.setArguments(bundle);

		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.replace(R.id.pray_list_main, prayListDetailFragment);
		ft.addToBackStack("prayListDetailFragment");
		ft.commit();
		mCursor.close();
		
	}

	private ArrayList<PrayModel> getAllPrayList(){
		ArrayList<PrayModel> prayList = Lists.newArrayList();
		PrayModel prayModel = null;
		mCursor = null;
		mCursor = mDbOpenHelper.getAllColumns();
		
		if(mCursor != null) {
			if(mCursor.getCount() > 0) {
				try {
					if(mCursor.moveToFirst()) {
						do {
							prayModel = new PrayModel(
									mCursor.getInt(mCursor.getColumnIndex("_id")),
									mCursor.getString(mCursor.getColumnIndex(Constant.DATE)),
									mCursor.getString(mCursor.getColumnIndex(Constant.THANK)),
									mCursor.getString(mCursor.getColumnIndex(Constant.REPENT)),
									mCursor.getString(mCursor.getColumnIndex(Constant.BLESS)),
									mCursor.getString(mCursor.getColumnIndex(Constant.WANT))
									);
							
							prayList.add(prayModel);
						}while(mCursor.moveToNext());
					}
				} catch(Exception e) {
					Log.e(Constant.TAG, ""+e);
				} finally {
					mCursor.close();
				}
			}
		}
		return prayList;
	}
	
	@Override
	public void onDestroyView() {
		if(mDbOpenHelper != null) {
			mDbOpenHelper.close();
			mDbOpenHelper = null;
		}
		super.onDestroyView();
	}
}