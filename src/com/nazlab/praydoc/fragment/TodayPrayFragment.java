package com.nazlab.praydoc.fragment;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nazlab.praydoc.MainActivity;
import com.nazlab.praydoc.R;
import com.nazlab.praydoc.db.DbOpenHelper;
import com.nazlab.praydoc.util.Constant;
import com.nazlab.praydoc.util.DateUtil;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class TodayPrayFragment extends Fragment {
	private TextView todayView;
	private ImageView amenView;
	private TextView dateView;
	
	private DateUtil dateUtil 	= new DateUtil();
	private DbOpenHelper mDbOpenHelper;
	private Typeface typeFace = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.today_pray, container, false);
		createView(view);
		return view;
	}
	
	private void createView(View view) {
		todayView = (TextView) view.findViewById(R.id.today_text);
		readPray();
		
		dateView = (TextView)view.findViewById(R.id.text_date);
		String date = dateUtil.getCurrentTime();
		typeFace = Typeface.createFromAsset(getActivity().getAssets(), Constant.FONT);
		dateView.setTypeface(typeFace, Typeface.BOLD);
		dateView.setText(date);
		amenView = (ImageView)view.findViewById(R.id.button_amen);
		amenView.setOnClickListener(amenListener);
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
	    super.setUserVisibleHint(isVisibleToUser);
	    // Make sure that we are currently visible
	    if (this.isVisible()) {
	    	readPray();
	    }
	}
	
	private void readPray() {
		StringBuffer stringBuffer = null;
		stringBuffer = new StringBuffer();
		MainActivity mainActivity = (MainActivity)getActivity();
		
		if(!mainActivity.getPreferences(Constant.THANK).equals(Constant.DEFAULT)) {
			String thankText = mainActivity.getPreferences(Constant.THANK);
			stringBuffer.append(thankText);
			stringBuffer.append("\r\n");
		}
		if(!mainActivity.getPreferences(Constant.REPENT).equals(Constant.DEFAULT)) {
			String repentText = mainActivity.getPreferences(Constant.REPENT);
			stringBuffer.append(repentText);
			stringBuffer.append("\r\n");
		}
		if(!mainActivity.getPreferences(Constant.BLESS).equals(Constant.DEFAULT)) {
			String blessText = mainActivity.getPreferences(Constant.BLESS);
			stringBuffer.append(blessText);
			stringBuffer.append("\r\n");
		}
		if(!mainActivity.getPreferences(Constant.WANT).equals(Constant.DEFAULT)) {
			String wantText = mainActivity.getPreferences(Constant.WANT);
			stringBuffer.append(wantText);
		}
		stringBuffer.append("\r\n");
		stringBuffer.append(Constant.LAST_COMMENT);
		todayView.setText(stringBuffer.toString());
	}
	
	private OnClickListener amenListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			ContentValues values = new ContentValues();
			values = insertData();
			
			// DB Create and Open
	        mDbOpenHelper = new DbOpenHelper(getActivity().getApplicationContext());
	        mDbOpenHelper.open();
	        mDbOpenHelper.insertColumn(values);
	        mDbOpenHelper.close();
	        Toast.makeText(getActivity().getApplicationContext(), Constant.SAVE_COMMENT, Toast.LENGTH_SHORT).show();
		}
	};
	
	private ContentValues insertData() {
		MainActivity mainActivity = (MainActivity)getActivity();
		ContentValues contentValues = new ContentValues();
		String date = dateUtil.currentDate();
		String thank = mainActivity.getPreferences(Constant.THANK);
		String repent = mainActivity.getPreferences(Constant.REPENT);
		String bless = mainActivity.getPreferences(Constant.BLESS);
		String want = mainActivity.getPreferences(Constant.WANT);
		
		contentValues.put(Constant.DATE, date);
				
		if(!thank.equals(Constant.DEFAULT)) {
			contentValues.put(Constant.THANK, thank);
		}
		if(!repent.equals(Constant.DEFAULT)) {
			contentValues.put(Constant.REPENT, repent);
		}
		if(!bless.equals(Constant.DEFAULT)) {
			contentValues.put(Constant.BLESS, bless);
		}
		if(!want.equals(Constant.DEFAULT)) {
			contentValues.put(Constant.WANT, want);
		}
		
		return contentValues;
	}
	
	@Override
	public void onDestroyView() {
		if(typeFace != null) {
			typeFace = null;
		}
		super.onDestroyView();
	}
}
